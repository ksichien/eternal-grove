CREATE TABLE IF NOT EXISTS Records (
    Id INT AUTO_INCREMENT PRIMARY KEY,
    Title varchar(255) NOT NULL,
    Artist varchar(255) NOT NULL
);

INSERT INTO Records (Artist, Title)
VALUES ('Cynic', 'Traced in Air');
INSERT INTO Records (Artist, Title)
VALUES ('Death', 'The Sound of Perseverance');
INSERT INTO Records (Artist, Title)
VALUES ('Dream Theater', 'Octavarium');
INSERT INTO Records (Artist, Title)
VALUES ('Have a Nice Life', 'Deathconsciousness');
INSERT INTO Records (Artist, Title)
VALUES ('Iced Earth', 'Burnt Offerings');
INSERT INTO Records (Artist, Title)
VALUES ('maudlin of the Well', 'Bath');
INSERT INTO Records (Artist, Title)
VALUES ('Opeth', 'Morningrise');

INSERT INTO Records (Artist, Title)
VALUES ('A Place To Bury Strangers', 'Exploding Head');
INSERT INTO Records (Artist, Title)
VALUES ('Boris', 'Pink');
INSERT INTO Records (Artist, Title)
VALUES ('Preoccupations', 'Viet Cong');
INSERT INTO Records (Artist, Title)
VALUES ('The Men', 'Leave Home');
INSERT INTO Records (Artist, Title)
VALUES ('Unwound', 'Unwound');

INSERT INTO Records (Artist, Title)
VALUES ('Alcest', 'Ecailles De Lune');
INSERT INTO Records (Artist, Title)
VALUES ('Deafheaven', 'Sunbather');
INSERT INTO Records (Artist, Title)
VALUES ('Harakiri for the Sky', 'Aokigahara');
INSERT INTO Records (Artist, Title)
VALUES ('Satyricon', 'Dark Medieval Times');
INSERT INTO Records (Artist, Title)
VALUES ('Summoning', 'Stronghold');

INSERT INTO Records (Artist, Title)
VALUES ('Bad Brains', 'Bad Brains');
INSERT INTO Records (Artist, Title)
VALUES ('Brand New', 'Science Fiction');
INSERT INTO Records (Artist, Title)
VALUES ('Cro-Mags', 'The Age of Quarrel');
INSERT INTO Records (Artist, Title)
VALUES ('Fugazi', 'Steady Diet of Nothing');
INSERT INTO Records (Artist, Title)
VALUES ('Gorilla Biscuits', 'Start Today');
INSERT INTO Records (Artist, Title)
VALUES ('Gouge Away', 'Burnt Sugar');
INSERT INTO Records (Artist, Title)
VALUES ('Gray Matter', 'Food for Thought');
INSERT INTO Records (Artist, Title)
VALUES ('Joy Division', 'Unknown Pleasures');
INSERT INTO Records (Artist, Title)
VALUES ('Judge', "Bringin' It Down");
INSERT INTO Records (Artist, Title)
VALUES ('meWithoutYou', 'Untitled');
INSERT INTO Records (Artist, Title)
VALUES ('Minor Threat', 'Out of Step');
INSERT INTO Records (Artist, Title)
VALUES ('Modern Life Is War', 'Witness');
INSERT INTO Records (Artist, Title)
VALUES ('Reatards', 'Grown Up, Fucked Up');
INSERT INTO Records (Artist, Title)
VALUES ('Shelter', 'Mantra');
INSERT INTO Records (Artist, Title)
VALUES ('Spanish Love Songs', 'Schmaltz');
INSERT INTO Records (Artist, Title)
VALUES ('The Faith', 'Subject to Change');
INSERT INTO Records (Artist, Title)
VALUES ('Together Pangea', 'Badillac');
INSERT INTO Records (Artist, Title)
VALUES ('Youth of Today', "We're Not In This Alone");

INSERT INTO Records (Artist, Title)
VALUES ('Emma Ruth Rundle', 'Some Heavy Ocean');
INSERT INTO Records (Artist, Title)
VALUES ('Julien Baker', 'Sprained Ankle');
INSERT INTO Records (Artist, Title)
VALUES ('Soccer Mommy', 'Clean');
INSERT INTO Records (Artist, Title)
VALUES ('Waxahatchee', 'Cerulean Salt');
