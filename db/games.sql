CREATE TABLE IF NOT EXISTS Games (
    Id char(36) PRIMARY KEY,
    Title varchar(255) NOT NULL,
    Developer varchar(255),
    Publisher varchar(255),
    Genre varchar(255) NOT NULL
);
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('a5c44810-378b-480b-bf22-7f3a94549fed', 'Aion: The Tower of Eternity', 'NCsoft', 'NCsoft', 'MMORPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('23278ffb-fade-4fdf-bd37-f12effee77a8', 'Final Fantasy XIV', 'Square Enix Business Division 5', 'Square Enix', 'MMORPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('06324979-7db3-42e3-9adf-229ad2c8ea87', 'Guild Wars', 'ArenaNet', 'NCsoft', 'MMORPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('2e0c0464-ea6c-499c-839a-8c73079bf253', 'The Lord of the Rings Online: Shadows of Angmar', 'Turbine', 'Codemasters', 'MMORPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('1f1a1704-b863-4827-8cc4-83f30c85083f', 'Warhammer Online: Age of Reckoning', 'Mythic Entertainment', 'Electronic Arts', 'MMORPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('422372c2-f082-4c23-9c33-c9c1456bac6c', 'World of Warcraft', 'Blizzard Entertainment', 'Blizzard Entertainment', 'MMORPG');

INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('50b6fc47-5b8b-4fde-9b00-f92df4f279e4', 'Baldurs Gate II: Shadows of Amn', 'BioWare', 'Black Isle Studios', 'RPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('ec8e47d8-775b-4869-94f9-7ddba78e39bb', 'Disco Elysium', 'ZA/UM', 'ZA/UM', 'RPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('91136992-13c7-44c3-a56e-e7becddbd6f6', 'Divinity: Original Sin 2', 'Larian Studios', 'Larian Studios', 'RPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('05efe5ec-3c22-430c-806f-5ac6b4fb3950', 'Dragon Age: Origins', 'BioWare', 'Electronic Arts', 'RPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('3dd685e4-ee8d-446c-b1d2-18fbf50927fd', 'Final Fantasy IX', 'Square', 'Square', 'RPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('6de49d3d-eb4d-4e3e-ad0c-a0ef557d3fbe', 'Mass Effect', 'BioWare', 'Electronic Arts', 'RPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('db260ad2-1e91-4337-8d74-86528d451892', 'Pathfinder: Kingmaker', 'Owlcat Games', 'Deep Silver', 'RPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('7e92cd8b-d730-4dfb-83b5-4e8ea6acdbda', 'Pillars of Eternity', 'Obsidian Entertainment', 'Paradox Interactive', 'RPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('d6bee13b-f5c5-40f3-9fbd-c4202dec0530', 'Shadowrun: Dragonfall', 'Harebrained Schemes', 'Harebrained Schemes', 'RPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('27246ce1-faf4-4355-b662-9d3b91976293', 'Shadowrun: Hong Kong', 'Harebrained Schemes', 'Harebrained Schemes', 'RPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('95e88c53-6c05-4968-b446-a2528fb40859', 'Vampire: The Masquerade - Bloodlines', 'Troika Games', 'Activision', 'RPG');

INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('bd001efd-d1b4-4613-9326-f654fa368517', 'Devil May Cry', 'Capcom', 'Capcom', 'Action-Adventure');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('918dc3d2-282c-4d4e-bdb2-a40784705f5b', 'Sekiro: Shadows Die Twice', 'FromSoftware', 'Activision', 'Action-Adventure');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('fbc843e0-58fc-4dc6-8f42-4ab74304256f', 'Tenchu: Stealth Assassins', 'Acquire', 'Activision', 'Action-Adventure');

INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('a1377a14-4738-4458-947f-5c1f54c9ec36', 'Dark Souls', 'FromSoftware', 'Namco Bandai Games', 'ARPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('4759ac44-fa68-4e18-8c86-5fa570579ccd', "Demon's Souls", 'FromSoftware', 'Namco Bandai Games', 'ARPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('1ca44e12-5bd9-414c-a750-24c3e3fce0a9', 'Deus Ex', 'Ion Storm', 'Eidos Interactive', 'ARPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('019d4f9b-abdb-4fe2-800a-d8e9dedfb8bb', 'Fallout 3', 'Bethesda Game Studios', 'Bethesda Softworks', 'ARPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('68f97e92-9963-4ea9-beee-495b82127937', 'Nier: Automata', 'PlatinumGames', 'Square Enix', 'ARPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('dc1e3929-6100-4396-8e0e-057c11d38077', 'Secret of Mana', 'Square', 'Square', 'ARPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('474b8034-43b3-4f5e-a034-01d591f49734', 'The Elder Scrolls IV: Oblivion', 'Bethesda Game Studios', 'Bethesda Softworks', 'ARPG');
INSERT INTO Games (Id, Title, Developer, Publisher, Genre)
VALUES ('551bd267-b490-4731-bf22-5916b7eee514', 'The Witcher 2: Assassins of Kings', 'CD Projekt Red', 'CD Projekt', 'ARPG');
