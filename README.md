# The Eternal Grove #

This project's primary purpose is to serve as a playground to experiment with various programming languages and DevOps tools.

It also serves as a personal website, hosted on AWS at <https://www.eternalgrove.net>.

The web application is built using a microservice architecture with:

- a React.JS frontend
- a .NET Core backend
- a MySQL database

It can be started using two different methods:

- Straight from the command-line.
- Using Docker.

There are also several Infrastructure-as-Code (IaC) configurations included in this project:

- A GitLab CI file to rebuild this project and run all tests every time this repository receives a new commit.
- A Jenkinsfile to integrate this project into a Jenkins Pipeline.
- A Python script using troposphere to provision resources for deployment on AWS CloudFormation.
- A Terraform configuration to provision and manage resources for deployment on AWS.

## Development using Command-line ##

### Database ###

1. Install and start the MySQL server:
    - `eternal-grove $ systemctl start mysqld`
2. Perform the database migrations:
    - `eternal-grove $ mysql --user=root --password=password --database=grove < db/games.sql`
    - `eternal-grove $ mysql --user=root --password=password --database=grove < db/records.sql`

### Backend ###

1. Install the application dependencies:
    - `backend $ dotnet restore`
2. Run the backend:
    - `backend $ dotnet run --project EternalGroveApi`
3. Visit the website:
    - <https://localhost:5001/api/v1/games>
    - <https://localhost:5001/api/v1/records>

### Frontend ###

1. Install the application dependencies:
    - `frontend $ npm install`
2. Run the frontend:
    - `frontend $ npm start`
3. Visit the website:
    - <http://localhost:3000>

### Testing ###

1. Run the games backend tests:
    - `backend $ dotnet test Tests/Games.UnitTests/Games.UnitTests.csproj`
2. Run the records backend tests:
    - `backend $ dotnet test Tests/Records.UnitTests/Records.UnitTests.csproj`
3. Run the frontend tests:
    - `frontend $ npm test`

## Development using Docker ##

1. Configure and start the containers:
    - `eternal-grove $ docker-compose up -d`
3. Visit the games backend:
    - <https://localhost:5001/api/v1/games>
4. Visit the records backend:
    - <https://localhost:5001/api/v1/records>
5. Visit the website:
    - <http://localhost>

## Deployment using Cloudformation ##

1. Install troposphere
    - `deployment/cloudformation $ pip3 install troposphere`
2. Generate the template file
    - `deployment/cloudformation $ python3 aws-deployment.py > template.yml`
3. Upload the template file to AWS CloudFormation

Note: for security reasons, all secrets have been redacted from the Python script.

## Deployment using Terraform ##

1. Configure terraform
    - `deployment/terraform $ terraform init`
2. Review changes
    - `deployment/terraform $ terraform plan`
3. Provision resources
    - `deployment/terraform $ terraform apply`
4. Destroy resources
    - `deployment/terraform $ terraform destroy`

Note: for security reasons, the variable (vars.tf) and secrets (secrets.tf) configuration files have not been checked into this git repository.
