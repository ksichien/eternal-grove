using EternalGroveApi;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Record.UnitTests
{
    public class RecordsControllerTest
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;

        public RecordsControllerTest()
        {
            // Arrange
            _server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            _client = _server.CreateClient();
        }

        [Fact]
        public async Task ReturnStatusCodeOK()
        {
            // Act
            var response = await _client.GetAsync("/api/v1/Records");
            // Assert
            response.EnsureSuccessStatusCode();
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}
