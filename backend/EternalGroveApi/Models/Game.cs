﻿namespace EternalGroveApi.Models
{
    public class Game
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Developer { get; set; }
        public string Publisher { get; set; }
        public string Genre { get; set; }
    }
}
