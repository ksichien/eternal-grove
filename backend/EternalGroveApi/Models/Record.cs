﻿namespace EternalGroveApi.Models
{
    public class Record
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Artist { get; set; }
    }
}
