﻿using EternalGroveApi.Contexts;
using EternalGroveApi.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EternalGroveApi.Services
{
    public class SeedService
    {
        public static async Task SeedGames(GameContext context)
        {
            if (!context.Games.Any())
            {
                string gamesJSON = File.ReadAllText(@"Data/games.json");
                List<Game> gamesList = JsonConvert.DeserializeObject<List<Game>>(gamesJSON);
                await context.AddRangeAsync(gamesList);
                await context.SaveChangesAsync();
            }
        }

        public static async Task SeedRecords(RecordContext context)
        {
            if (!context.Records.Any())
            {
                string recordsJSON = File.ReadAllText(@"Data/records.json");
                List<Record> recordsList = JsonConvert.DeserializeObject<List<Record>>(recordsJSON);
                await context.AddRangeAsync(recordsList);
                await context.SaveChangesAsync();
            }
        }
    }
}
