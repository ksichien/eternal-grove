import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import MusicComponent from './music';
import RecordListComponent, { Record, parseRecords } from './recordList';

describe('<MusicComponent />', () => {
  const flushPromises = () => new Promise(resolve => setImmediate(resolve));
  let wrapper: ShallowWrapper;
  beforeEach(async () => {
    wrapper = shallow(<MusicComponent />);
    await flushPromises();
    wrapper.update();
  });
  it('renders one recordlist component', async () => {
    expect(wrapper.contains(<RecordListComponent />)).toBeTruthy;
  });
  it('displays a record', async () => {
    expect(wrapper.contains(<th key={`th-artist-0`} scope="row">Cynic</th>)).toBeTruthy;
  });
});

describe('parseRecords', () => {
  it('parses and sorts a list of records', () => {
    const input: any = [
      {
        "artist": "maudlin of the Well",
        "title": "Bath"
      },
      {
        "artist": "Death",
        "title": "The Sound of Perseverance"
      },
      {
        "artist": "Cynic",
        "title": "Traced in Air"
      }
    ];
    const output: Record[] = [
      {
        "artist": "Cynic",
        "title": "Traced in Air"
      },
      {
        "artist": "Death",
        "title": "The Sound of Perseverance"
      },
      {
        "artist": "maudlin of the Well",
        "title": "Bath"
      }
    ];
    expect(parseRecords(input)).toEqual(output);
  });
});
