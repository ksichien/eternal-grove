import React, { Component } from 'react';
import './music.css';
import RecordListComponent from './recordList';

class MusicComponent extends Component<{}, {}> {
  render() {
    return (
      <section className="bg-music my-5">
        <div className="text-center text-light">
          <h1 className="display-3 py-3">Music</h1>
          <div className="py-4">
            <p className="lead">You can find a list of notable records I've listened to on this page.</p>
            <RecordListComponent />
          </div>
        </div>
      </section>
    );
  }
}

export default MusicComponent;
