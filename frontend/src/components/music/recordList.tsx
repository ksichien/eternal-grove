import React, { Component } from 'react';
import Pagination from 'react-js-pagination';
import { Table } from 'react-bootstrap';
import axios, { AxiosResponse } from 'axios';
import * as fallbackJSON from './records.json';
import './music.css';

export interface Record {
  title: string;
  artist: string;
}

interface RecordState {
  activePage: number;
  currentTableData: JSX.Element[];
  records: Record[];
  tableData: JSX.Element[];
}

class RecordListComponent extends Component<{}, RecordState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      activePage: 1,
      currentTableData: [],
      records: [],
      tableData: []
    }
  }

  componentDidMount() {
    const uri = 'http://localhost:5000/api/v1/records';
    getRecords(uri)
      .then((records: any) => parseRecords(records))
      .then((parsedRecords: Record[]) => {
        const allData = parsedRecords.map((record: Record, index: number) => {
          return (
            <tr key={`tr-record-${index}`}>
              <th key={`th-artist-${index}`} scope="row">{record.artist}</th>
              <td key={`td-title-${index}`}>{record.title}</td>
            </tr>
          )
        });
        this.setState({
          currentTableData: allData.slice(0, 10),
          records: parsedRecords,
          tableData: allData
        });
      })
      .catch((err: Error) => {
        this.setState(() => {
          throw new Error(`There was an issue processing the backend service data: ${err.message}`);
        });
      });
  }

  handlePageChange = (pageNumber: number) => {
    const baseNumber = pageNumber - 1;
    const begin = baseNumber * 10;
    const end = begin + 10;
    this.setState({
      activePage: pageNumber,
      currentTableData: this.state.tableData.slice(begin, end)
    });
  }

  render() {
    return (
      <div className="container vh-70">
        <Table borderless responsive="lg" variant="dark" className="table-transparent">
          <thead>
            <tr>
              <th scope="col">Artist</th>
              <th scope="col">Title</th>
            </tr>
          </thead>
          <tbody>
            {this.state.currentTableData}
          </tbody>
        </Table>
        <Pagination
          activePage={this.state.activePage}
          totalItemsCount={this.state.tableData.length}
          itemsCountPerPage={10}
          onChange={this.handlePageChange}
          itemClass="page-item"
          linkClass="page-link"
        />
      </div>
    );
  }
}

export function parseRecords(data: any): Record[] {
  let records: Record[] = [];
  for (let i = 0; i < data.length; i++) {
    const record = { artist: data[i].artist, title: data[i].title };
    records.push(record);
  }
  return records.sort((a: Record, b: Record) => {
    return a.artist > b.artist ? 1 : b.artist > a.artist ? -1 : 0
  });
}

export function getRecords(uri: string): Promise<any> {
  return axios.get(uri)
    .then((res: AxiosResponse<any>) => { return res.data })
    .catch((err: Error) => { return fallbackJSON.records });
}

export default RecordListComponent;
