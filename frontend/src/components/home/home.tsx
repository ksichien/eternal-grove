import React from 'react';
import './home.css';
import ParticleComponent from './particle';

const HomeComponent: React.FC = () => {
  return (
    <section className="bg-home">
      <ParticleComponent />
      <div className="text-light vertical-center">
        <h1 className="display-4">Kenny Sichien</h1>
        <p className="lead">System Administrator</p>
      </div>
    </section>
  );
}

export default HomeComponent;
