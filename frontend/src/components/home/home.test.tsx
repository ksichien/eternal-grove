import React from 'react';
import { shallow } from 'enzyme';
import HomeComponent from './home';
import ParticleComponent from './particle';

describe('<HomeComponent />', () => {
  it('renders one particle subcomponent', () => {
    const wrapper = shallow(<HomeComponent />);
    expect(wrapper.contains(<ParticleComponent />)).toBeTruthy;
  });
});
