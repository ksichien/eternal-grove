import React from 'react';
import { shallow } from 'enzyme';
import ErrorBoundary from './ErrorBoundary';

describe('<ErrorBoundary />', () => {
  it('renders the error', () => {
    const wrapper = shallow(<ErrorBoundary>{throwError}</ErrorBoundary>);
    expect(wrapper.contains(<h3 className="display-4">Error!</h3>)).toBeTruthy;
  });
});

function throwError() {
    throw new Error('Error!');
}
