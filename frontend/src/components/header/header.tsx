import React, { Component } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGitlab, faLinkedin } from '@fortawesome/free-brands-svg-icons';

class HeaderComponent extends Component {
  render() {
    return (
      <Navbar bg="light" expand="lg" fixed="top">
        <Navbar.Brand href="/">Eternal Grove</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="/games">Games</Nav.Link>
            <Nav.Link href="/music">Music</Nav.Link>
            <Nav.Link href="/about">About</Nav.Link>
          </Nav>
          <Nav className="ml-auto">
            <Nav.Link href="https://gitlab.com/ksichien"><FontAwesomeIcon icon={faGitlab} size="lg" /></Nav.Link>
            <Nav.Link href="https://linkedin.com/in/ksichien"><FontAwesomeIcon icon={faLinkedin} size="lg" /></Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default HeaderComponent;
