import React, { Component } from 'react';
import './not-found.css';

class NotFoundComponent extends Component {
  render() {
    return (
      <section className="bg-not-found">
        <div className="text-light vertical-center">
          <h1 className="display-4">404 not found</h1>
          <p className="lead">Go back to the <a className="badge badge-light" href="/">home</a> page</p>
        </div>
      </section>
    );
  }
}

export default NotFoundComponent;
