import React, { Component } from 'react';
import Pagination from 'react-js-pagination';
import { Table } from 'react-bootstrap';
import axios, { AxiosResponse } from 'axios';
import * as fallbackJSON from './games.json';
import './game.css';

export interface Game {
  title: string;
  developer?: string;
  publisher?: string;
  genre: string;
}

interface GameState {
  activePage: number;
  currentTableData: JSX.Element[];
  games: Game[];
  tableData: JSX.Element[];
}

class GameListComponent extends Component<{}, GameState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      activePage: 1,
      currentTableData: [],
      games: [],
      tableData: []
    }
  }

  componentDidMount() {
    const uri = 'http://localhost:5000/api/v1/games';
    getGames(uri)
      .then((games: any) => parseGames(games))
      .then((parsedGames: Game[]) => {
        const allData = parsedGames.map((game: Game, index: number) => {
          return (
            <tr key={`tr-game-${index}`}>
              <th key={`th-title-${index}`} scope="row">{game.title}</th>
              <td key={`td-developer-${index}`}>{game.developer ? game.developer : 'Unknown'}</td>
              <td key={`td-publisher-${index}`}>{game.publisher ? game.publisher : 'Unknown'}</td>
              <td key={`td-genre-${index}`}>{game.genre}</td>
            </tr>
          )
        });
        this.setState({
          currentTableData: allData.slice(0, 10),
          games: parsedGames,
          tableData: allData
        });
      })
      .catch((err: Error) => {
        this.setState(() => {
          throw new Error(`There was an issue processing the backend service data: ${err.message}`);
        });
      });
  }

  handlePageChange = (pageNumber: number) => {
    const baseNumber = pageNumber - 1;
    const begin = baseNumber * 10;
    const end = begin + 10;
    this.setState({
      activePage: pageNumber,
      currentTableData: this.state.tableData.slice(begin, end)
    });
  }

  render() {
    return (
      <div className="container vh-70">
        <Table borderless responsive="lg" variant="dark" className="table-transparent">
          <thead>
            <tr>
              <th scope="col">Title</th>
              <th scope="col">Developer</th>
              <th scope="col">Publisher</th>
              <th scope="col">Genre</th>
            </tr>
          </thead>
          <tbody>
            {this.state.currentTableData}
          </tbody>
        </Table>
        <Pagination
          activePage={this.state.activePage}
          totalItemsCount={this.state.tableData.length}
          itemsCountPerPage={10}
          onChange={this.handlePageChange}
          itemClass="page-item"
          linkClass="page-link"
        />
      </div>
    );
  }
}

export function parseGames(data: any): Game[] {
  let games: Game[] = [];
  for (let i = 0; i < data.length; i++) {
    const game = { title: data[i].title, developer: data[i].developer, publisher: data[i].publisher, genre: data[i].genre };
    games.push(game);
  }
  return games.sort((a: Game, b: Game) => {
    return b.genre > a.genre ? 1 : a.genre > b.genre ? -1 : 0
  });
}

export function getGames(uri: string): Promise<any> {
  return axios.get(uri)
    .then((res: AxiosResponse<any>) => { return res.data })
    .catch((err: Error) => { return fallbackJSON.games });
}

export default GameListComponent;
