import React, { Component } from 'react';
import './game.css';
import GameListComponent from './gameList';

class GameComponent extends Component<{}, {}> {
  render() {
    return (
      <section className="bg-game my-5">
        <div className="text-center text-light">
          <h1 className="display-3 py-3">Games</h1>
          <div className="py-4">
            <p className="lead">You can find a list of notable games I've played on this page.</p>
            <GameListComponent />
          </div>
        </div>
      </section>
    );
  }
}

export default GameComponent;
