import React from 'react';
import { shallow, ShallowWrapper } from 'enzyme';
import AboutComponent from './about';
import AuthorComponent from './author';
import SkillsComponent from './skills';
import WebsiteComponent from './website';
import ContactComponent from './contact';

describe('<AboutComponent />', () => {
  let wrapper: ShallowWrapper;
  beforeEach(async () => {
    wrapper = shallow(<AboutComponent />);
  });
  it('renders the author subcomponent', () => {
    expect(wrapper.contains(<AuthorComponent />)).toBeTruthy;
  });
  it('renders the website subcomponent', () => {
    expect(wrapper.contains(<WebsiteComponent />)).toBeTruthy;
  });
  it('renders the skills subcomponent', () => {
    expect(wrapper.contains(<SkillsComponent />)).toBeTruthy;
  });
  it('renders the contact subcomponent', () => {
    expect(wrapper.contains(<ContactComponent />)).toBeTruthy;
  });
});
