import React, { Component } from 'react';
import { Table } from 'react-bootstrap';

class SkillsComponent extends Component {
  render() {
    const categories: string[] = [
      'Cloud Services',
      'Hypervisors',
      'Linux Administration',
      'Windows Administration',
      'macOS Administration',
      'Automation Tools',
      'Continuous Integration',
      'Programming Languages',
      'Scripting Languages'
    ];
    const technologies: string[] = [
      'AWS, Azure',
      'Hyper-V, VMware ESXi',
      'Bind, Grafana, Prometheus, OpenLDAP',
      'Active Directory, Dynamics NAV, Office 365',
      'Jamf Pro',
      'Ansible, CloudFormation, Docker, Terraform',
      'GitLab CI, Jenkins',
      'C#, JavaScript, Python',
      'Bash, PowerShell'
    ];
    const tableData = categories.map((category, index) => {
      return <tr key={`tr-${index}`}><th className="text-right w-50" key={`th-${index}`}>{category}</th><td className="text-left w-50" key={`td-${index}`}>{technologies[index]}</td></tr>
    });
    return (
      <section>
        <h3 className="display-4 py-4">Skills</h3>
        <div className="bg-dark py-4">
          <div className="container">
            <Table borderless responsive="lg" variant="dark">
              <tbody>
                {tableData}
              </tbody>
            </Table>
          </div>
        </div>
      </section>
    );
  }
}

export default SkillsComponent;
