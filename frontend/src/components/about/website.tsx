import React, { Component } from 'react';

class WebsiteComponent extends Component {
  render() {
    return (
      <section>
        <h3 className="display-4 py-4">This Website</h3>
        <div className="bg-dark py-4">
          <div className="container">
            <p>This web application is built using a microservice architecture with one frontend, two backends and a database.</p>
            <p>The project's source code is hosted on a <a className="badge badge-light" href="https://gitlab.com/ksichien/eternal-grove">GitLab</a> repository.</p>
            <p>Whenever a push is made to this repository, GitLab CI will trigger a <a className="badge badge-light" href="https://gitlab.com/ksichien/eternal-grove/blob/master/.gitlab-ci.yml">CI/CD</a> process which executes unit tests and then creates production builds.</p>
            <p>Finally, if a push is made to the <a className="badge badge-light" href="https://gitlab.com/ksichien/eternal-grove/tree/release">release</a> branch, the CI/CD process also deploys the updated production build on AWS.</p>
            <p>Credit for all background images goes to various artists from <a className="badge badge-light" href="https://unsplash.com">Unsplash</a> (@hikendal, @blurrystock, @alexkixa, @trommelkopf and @davidkovalenkoo).</p>
          </div>
        </div>
      </section>
    );
  }
}

export default WebsiteComponent;
