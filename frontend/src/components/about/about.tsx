import React, { Component } from 'react';
import './about.css';
import AuthorComponent from './author';
import SkillsComponent from './skills';
import WebsiteComponent from './website';
import ContactComponent from './contact';

class AboutComponent extends Component {
  render() {
    return (
      <section className="bg-about my-5">
        <div className="text-center text-light">
          <AuthorComponent />
          <WebsiteComponent />
          <SkillsComponent />
          <ContactComponent />
        </div>
      </section>
    );
  }
}

export default AboutComponent;
