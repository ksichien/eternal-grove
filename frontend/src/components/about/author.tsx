import React, { Component } from 'react';

class AuthorComponent extends Component {
  render() {
    return (
      <section>
        <h3 className="display-4 py-4">About the author</h3>
        <div className="bg-dark py-4">
          <div className="container">
            <p>I'm a System Administrator with several years of experience.</p>
            <p>I've administered Linux as well as Windows systems, on-premise and in the cloud.</p>
            <p>I enjoy experimenting with DevOps technologies at home and applying them in a professional setting.</p>
          </div>
        </div>
      </section>
    );
  }
}

export default AuthorComponent;
