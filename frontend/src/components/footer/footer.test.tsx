import React from 'react';
import { Nav } from 'react-bootstrap';
import { shallow } from 'enzyme';
import FooterComponent from './footer';

describe('<FooterComponent />', () => {
  it('renders one nav item', () => {
    const year = `${new Date().getFullYear()}`;
    const wrapper = shallow(<FooterComponent />);
    expect(wrapper.contains(<Nav.Item>The Eternal Grove &copy; {year}</Nav.Item>)).toBeTruthy;
  });
});
