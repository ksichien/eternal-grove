import React, { Component } from 'react';
import { Navbar, Nav } from 'react-bootstrap';

class FooterComponent extends Component {
  render() {
    const year = `${new Date().getFullYear()}`;
    return (
      <Navbar bg="light" fixed="bottom">
        <Nav className="m-auto">
          <Nav.Item>The Eternal Grove &copy; 2018-{year}</Nav.Item>
        </Nav>
      </Navbar>
    );
  }
}

export default FooterComponent;
