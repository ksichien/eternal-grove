import React from 'react';
import { shallow } from 'enzyme';
import App from './App';
import HeaderComponent from './components/header/header';
import HomeComponent from './components/home/home';
import FooterComponent from './components/footer/footer';

describe('<AboutComponent />', () => {
  it('renders three subcomponents', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.contains(<HeaderComponent />)).toBeTruthy;
    expect(wrapper.contains(<HomeComponent />)).toBeTruthy;
    expect(wrapper.contains(<FooterComponent />)).toBeTruthy;
  });
});
