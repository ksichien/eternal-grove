import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import HeaderComponent from './components/header/header';
import HomeComponent from './components/home/home';
import GameComponent from './components/game/game';
import MusicComponent from './components/music/music';
import AboutComponent from './components/about/about';
import NotFoundComponent from './components/not-found/not-found';
import FooterComponent from './components/footer/footer';
import ErrorBoundary from './components/errorboundary/ErrorBoundary';

class App extends Component {
  render() {
    return (
      <Router>
        <header>
          <HeaderComponent />
        </header>
        <main role="main">
          <ErrorBoundary>
            <Switch>
              <Route exact path="/" component={HomeComponent} />
              <Route path="/games" component={GameComponent} />
              <Route path="/music" component={MusicComponent} />
              <Route path="/about" component={AboutComponent} />
              <Route component={NotFoundComponent} />
            </Switch>
          </ErrorBoundary>
        </main>
        <footer>
          <FooterComponent />
        </footer>
      </Router>
    );
  }
}

export default App;
