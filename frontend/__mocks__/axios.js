export default {
    get: jest.fn(() => Promise.resolve([{
        "title": "Final Fantasy XIV",
        "developer": "Square Enix Business Division 5",
        "publisher": "Square Enix",
        "genre": "MMORPG"
    }, {
        "title": "Darkfall Online",
        "genre": "MMORPG"
    }]))
};
