resource "aws_s3_bucket" "eg-html-s3" {
  bucket = var.s3_dns
  acl = "public-read"

  website {
    index_document = "index.html"
    error_document = "index.html"
  }
}

resource "aws_cloudfront_distribution" "eg-html-cfd" {
  origin {
    domain_name = "${aws_s3_bucket.eg-html-s3.bucket_regional_domain_name}"
    origin_id   = var.cf_custom_origin_id

    custom_origin_config {
      http_port = 80
      https_port = 443
      origin_ssl_protocols = [ "SSLv3", "TLSv1", "TLSv1.1", "TLSv1.2" ]
      origin_protocol_policy = "http-only"
    }
  }

  enabled = true
  is_ipv6_enabled = true
  default_root_object = "index.html"
  aliases = [ var.r53_hosted_zone_name, var.s3_dns ]

  custom_error_response {
    error_code = "404"
    response_code = "200"
    response_page_path = "/index.html"
  }

  default_cache_behavior {
    allowed_methods = ["GET", "HEAD"]
    cached_methods = ["GET", "HEAD"]
    target_origin_id = var.cf_custom_origin_id

    forwarded_values {
      query_string = false

      cookies {
	forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl = 0
    default_ttl = 3600
    max_ttl = 86400
  }

  price_class = "PriceClass_100"

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn = var.cf_acm_arn
    minimum_protocol_version = "TLSv1"
    ssl_support_method = "sni-only"
  }
}

resource "aws_route53_record" "eg-html-r53" {
  zone_id = var.r53_hosted_zone_id
  name = var.s3_dns
  type = "CNAME"
  ttl = "900"
  records = ["${aws_cloudfront_distribution.eg-html-cfd.domain_name}"]
}

resource "aws_route53_record" "eg-html-alias" {
  zone_id = var.r53_hosted_zone_id
  name = var.r53_hosted_zone_name
  type = "A"

  alias {
    name = "${aws_cloudfront_distribution.eg-html-cfd.domain_name}"
    zone_id = "${aws_cloudfront_distribution.eg-html-cfd.hosted_zone_id}"
    evaluate_target_health = false
  }
}
