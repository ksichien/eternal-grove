resource "aws_key_pair" "eg-api-keypair" {
  key_name   = "eg-api-keypair"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD3F6tyPEFEzV0LX3X8BsXdMsQz1x2cEikKDEY0aIj41qgxMCP/iteneqXSIFZBp5vizPvaoIR3Um9xK7PGoW8giupGn+EPuxIA4cDM4vzOqOkiMPhz5XK0whEjkVzTo4+S0puvDZuwIsdiW9mxhJc7tgBNL0cYlWSYVkz4G/fslNfRPW5mYAM49f4fhtxPb5ok4Q2Lg9dPKVHO/Bgeu5woMc7RY0p1ej6D4CKFE6lymSDJpW0YHX/wqE9+cfEauh7xZcG0q9t2ta6F6fmX0agvpFyZo8aFbXeUBr7osSCJNgvavWbM/06niWrOvYX2xwWdhXmXSrbX8ZbabVohBK41 email@example.com"
}

resource "aws_instance" "eg-api-ec2" {
  ami = var.api_ami
  instance_type = "t2.micro"
  key_name = "${aws_key_pair.eg-api-keypair.key_name}"
  vpc_security_group_ids = [ "${aws_security_group.eg-ssh-sg.id}", "${aws_security_group.eg-http-sg.id}", "${aws_security_group.eg-https-sg.id}" ]
  subnet_id = "${aws_subnet.eg-api-sn.id}"
}

resource "aws_route53_record" "eg-api-r53" {
  zone_id = var.r53_hosted_zone_id
  name = var.s3_dns
  type = "A"
  ttl = "900"
  records = [ "${aws_instance.eg-api-ec2.domain_name}" ]
}
