resource "aws_vpc" "eg-vpc" {
  cidr_block = var.vpc_cidr
}

resource "aws_subnet" "eg-api-sn" {
  vpc_id = "${aws_vpc.eg-vpc.id}"
  cidr_block = var.api_sn_cidr
}

resource "aws_internet_gateway" "eg-igw" {
  vpc_id = "${aws_vpc.eg-vpc.id}"
}

resource "aws_route_table" "eg-rt" {
  vpc_id = "${aws_vpc.eg-vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.eg-igw.id}"
  }
}

resource "aws_route_table_association" "eg-api-sn-assoc" {
  subnet_id = "${aws_subnet.eg-api-sn.id}"
  route_table_id = "${aws_route_table.eg-rt.id}"
}

resource "aws_security_group" "eg-api-ssh-sg" {
  name = "eg-api-ssh-sg"
  vpc_id = "${aws_vpc.eg-vpc.id}"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [ var.office_ip ]
  }
  egress {
   from_port = 22
   to_port = 22
   protocol = "tcp"
   cidr_blocks = [ var.office_ip ] 
  }
}

resource "aws_security_group" "eg-api-http-sg" {
  name = "eg-api-http-sg"
  vpc_id = "${aws_vpc.eg-vpc.id}"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }
  egress {
   from_port = 80
   to_port = 80
   protocol = "tcp"
   cidr_blocks = [ "0.0.0.0/0" ] 
  }
}

resource "aws_security_group" "eg-api-https-sg" {
  name = "eg-api-https-sg"
  vpc_id = "${aws_vpc.eg-vpc.id}"

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }
  egress {
   from_port = 443
   to_port = 443
   protocol = "tcp"
   cidr_blocks = [ "0.0.0.0/0" ] 
  }
}

resource "aws_eip" "eg-api-eip" {
  instance = "${aws_instance.eg-api-ec2.id}"
  vpc = true
}
