#!/usr/bin/env python3
## imports ##
from troposphere import GetAtt, Join, Output, Parameter, Ref, Select, Split, Template
from troposphere.ec2 import EIP, Instance, SecurityGroup, SecurityGroupRule
from troposphere.ec2 import InternetGateway, NetworkAcl, PortRange, RouteTable, Subnet, VPC
from troposphere.ec2 import NetworkAclEntry, Route, SubnetNetworkAclAssociation, SubnetRouteTableAssociation, VPCGatewayAttachment
import troposphere.cloudfront as cf
import troposphere.elasticloadbalancing as elb
import troposphere.route53 as r53
import troposphere.s3 as s3

## template definition ##
t = Template()
t.set_version("2010-09-09")
t.set_description("""\
An AWS CloudFormation template that generates infrastructure for a web application. \
It creates the following resources: \
- 1 vpc \
- 1 public subnet \
- 1 network acl with 6 entries \
- 1 cloudfront distribution \
- 1 s3 bucket for a static website \
- 1 ec2 instance for the api service \
- 2 security groups for the ec2 instances \
- 3 route53 dns records for the cf distribution and the ec2 instance \
""")

## variables ##
# general
region = "eu-west-1"

# default parameters
hosted_zone_param = "eternalgrove.net"
hosted_zone_id_param = "XXXXXXXXXXXXXX"
ec2_keyname_param = "eternalgrove-backend"
acm_cert_arn_param = "arn:aws:acm:us-east-1:XXXXXXXXXXXX:certificate/XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"

# networking
vpc_cidr = "172.16.0.0/16"
subnet_cidr = "172.16.0.0/24"
office_ip = "0.0.0.0/0" # replace with office public IP address

# route53
api_dns = "api.eternalgrove.net"
s3_dns = "www.eternalgrove.net"

# ec2
api_instances = 1
api_port = 80
image_id = "ami-XXXXXXXXXXXXXXXXX" # eu-west-1 Ubuntu 18.04 LTS amd64 hvm:ebs-ssd
instance_type = "t2.micro"

## parameters ##
hosted_zone = t.add_parameter(Parameter(
    "HostedZone",
    Default=hosted_zone_param,
    Description="DNS name of an existing Route 53 hosted zone",
    Type="String",
))

hosted_zone_id = t.add_parameter(Parameter(
    "HostedZoneId",
    Default=hosted_zone_id_param,
    Description="ID of an existing Route 53 hosted zone",
    Type="String",
))

ec2_keyname = t.add_parameter(Parameter(
    "KeyName",
    Default=ec2_keyname_param,
    Description="Name of an existing EC2 KeyPair to enable SSH access to the instances",
    Type="AWS::EC2::KeyPair::KeyName",
))
acm_cert_arn = t.add_parameter(Parameter(
    "AcmCertArn",
    Default=acm_cert_arn_param,
    Description="ARN of an existing ACM Certificate for CloudFront",
    Type="String",
))

## resources ##
s3bucket = t.add_resource(s3.Bucket(
    "S3Bucket",
    AccessControl=s3.PublicRead,
    BucketName=s3_dns,
    WebsiteConfiguration=s3.WebsiteConfiguration(
        IndexDocument="index.html",
        ErrorDocument="index.html"
    )
))

cloudfront = t.add_resource(cf.Distribution(
    "CFDistribution",
    DistributionConfig=cf.DistributionConfig(
        Aliases=[s3_dns],
        Enabled=True,
        DefaultRootObject="index.html",
        HttpVersion="http2",
        Origins=[cf.Origin(
            Id="S3 Origin",
            DomainName=Select(1, Split("//", GetAtt("S3Bucket", "WebsiteURL"))),
            CustomOriginConfig=cf.CustomOriginConfig(
                HTTPPort="80",
                HTTPSPort="443",
                OriginProtocolPolicy="http-only"
            )
        )],
        DefaultCacheBehavior=cf.DefaultCacheBehavior(
            AllowedMethods=["GET", "HEAD"],
            ForwardedValues=cf.ForwardedValues(
                QueryString=False
            ),
            TargetOriginId="S3 Origin",
            ViewerProtocolPolicy="redirect-to-https"
        ),
        CustomErrorResponses=[cf.CustomErrorResponse(
            ErrorCode=404,
            ResponseCode=200,
            ResponsePagePath="/index.html"
        )],
        ViewerCertificate=cf.ViewerCertificate(
            AcmCertificateArn=Ref(acm_cert_arn),
            SslSupportMethod="sni-only"
        )
    )
))

vpc = t.add_resource(VPC(
    "VPC",
    CidrBlock=vpc_cidr
))

route_table = t.add_resource(RouteTable(
    "RouteTable",
    VpcId=Ref(vpc)
))

igw = t.add_resource(InternetGateway(
    "InternetGateway"
))

igw_attach = t.add_resource(VPCGatewayAttachment(
    "AttachGateway",
    VpcId=Ref(vpc),
    InternetGatewayId=Ref(igw)
))

route = t.add_resource(Route(
    "Route",
    DependsOn="AttachGateway",
    GatewayId=Ref(igw),
    DestinationCidrBlock="0.0.0.0/0",
    RouteTableId=Ref(route_table),
))

network_acl = t.add_resource(NetworkAcl(
    "NetworkAcl",
    VpcId=Ref(vpc)
))

acl_ssh = t.add_resource(NetworkAclEntry(
    "NetworkAclEntryInboundSSH",
    NetworkAclId=Ref(network_acl),
    RuleNumber="110",
    Protocol="6",
    PortRange=PortRange(To="22", From="22"),
    Egress="false",
    RuleAction="allow",
    CidrBlock=office_ip
))

acl_http = t.add_resource(NetworkAclEntry(
    "NetworkAclEntryInboundHTTP",
    NetworkAclId=Ref(network_acl),
    RuleNumber="120",
    Protocol="6",
    PortRange=PortRange(To="80", From="80"),
    Egress="false",
    RuleAction="allow",
    CidrBlock="0.0.0.0/0"
))

acl_https = t.add_resource(NetworkAclEntry(
    "NetworkAclEntryInboundHTTPS",
    NetworkAclId=Ref(network_acl),
    RuleNumber="130",
    Protocol="6",
    PortRange=PortRange(To="443", From="443"),
    Egress="false",
    RuleAction="allow",
    CidrBlock="0.0.0.0/0"
))

acl_other = t.add_resource(NetworkAclEntry(
    "NetworkAclEntryInboundOther",
    NetworkAclId=Ref(network_acl),
    RuleNumber="140",
    Protocol="6",
    PortRange=PortRange(To="65535", From="1024"),
    Egress="false",
    RuleAction="allow",
    CidrBlock="0.0.0.0/0"
))

acl_outbound_ssh = t.add_resource(NetworkAclEntry(
    "NetworkAclEntryOutboundSSH",
    NetworkAclId=Ref(network_acl),
    RuleNumber="210",
    Protocol="6",
    PortRange=PortRange(To="22", From="22"),
    Egress="true",
    RuleAction="allow",
    CidrBlock=office_ip
))

acl_outbound_http = t.add_resource(NetworkAclEntry(
    "NetworkAclEntryOutboundHTTP",
    NetworkAclId=Ref(network_acl),
    RuleNumber="220",
    Protocol="6",
    PortRange=PortRange(To="80", From="80"),
    Egress="true",
    RuleAction="allow",
    CidrBlock="0.0.0.0/0"
))

acl_outbound_https = t.add_resource(NetworkAclEntry(
    "NetworkAclEntryOutboundHTTPS",
    NetworkAclId=Ref(network_acl),
    RuleNumber="230",
    Protocol="6",
    PortRange=PortRange(To="443", From="443"),
    Egress="true",
    RuleAction="allow",
    CidrBlock="0.0.0.0/0"
))

acl_outbound_other = t.add_resource(NetworkAclEntry(
    "NetworkAclEntryOutboundOther",
    NetworkAclId=Ref(network_acl),
    RuleNumber="240",
    Protocol="6",
    PortRange=PortRange(To="65535", From="1024"),
    Egress="true",
    RuleAction="allow",
    CidrBlock="0.0.0.0/0"
))

subnet = t.add_resource(Subnet(
    "ELBSubnet",
    CidrBlock=subnet_cidr,
    VpcId=Ref(vpc)
))

subnet_rt_associate = t.add_resource(SubnetRouteTableAssociation(
    "ELBSubnetRouteTableAssociation",
    SubnetId=Ref(subnet),
    RouteTableId=Ref(route_table),
))

subnet_acl_associate = t.add_resource(SubnetNetworkAclAssociation(
    "ELBSubnetNetworkAclAssociation",
    SubnetId=Ref(subnet),
    NetworkAclId=Ref(network_acl),
))

ec2_sg_ssh = t.add_resource(SecurityGroup(
    "EC2SecurityGroupSSH",
    SecurityGroupIngress=[
        SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="22",
            ToPort="22",
            CidrIp=office_ip)],
    SecurityGroupEgress=[
        SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="22",
            ToPort="22",
            CidrIp=office_ip)],
    VpcId=Ref(vpc),
    GroupDescription="Security group for allowing SSH traffic to the EC2 instances"
))

ec2_sg_http = t.add_resource(SecurityGroup(
    "EC2SecurityGroupHTTP",
    SecurityGroupIngress=[
        SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="80",
            ToPort="80",
            CidrIp="0.0.0.0/0")],
    SecurityGroupEgress=[
        SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="80",
            ToPort="80",
            CidrIp="0.0.0.0/0")],
    VpcId=Ref(vpc),
    GroupDescription="Security group for allowing HTTP traffic to the EC2 instances"
))

ec2_sg_https = t.add_resource(SecurityGroup(
    "EC2SecurityGroupHTTPS",
    SecurityGroupIngress=[
        SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="443",
            ToPort="443",
            CidrIp="0.0.0.0/0")],
    SecurityGroupEgress=[
        SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="443",
            ToPort="443",
            CidrIp="0.0.0.0/0")],
    VpcId=Ref(vpc),
    GroupDescription="Security group for allowing HTTPS traffic to the EC2 instances"
))

ec2_instances = []

for i in range(api_instances):
    instance = t.add_resource(Instance(
        "EC2Instance{0}".format(i),
        ImageId=image_id,
        InstanceType=instance_type,
        KeyName=Ref(ec2_keyname),
        SecurityGroupIds=[Ref(ec2_sg_http), Ref(ec2_sg_https), Ref(ec2_sg_ssh)],
        SubnetId=Ref(subnet)
    ))
    eip = t.add_resource(EIP(
        "EIP{0}".format(i),
        Domain="vpc",
        InstanceId=Ref(instance)
    ))
    ec2_instances.append(instance)

alias_record = t.add_resource(r53.RecordSetType(
    "cfAliasRecord",
    HostedZoneName=Join("", [Ref(hosted_zone), "."]),
    Comment="dns alias for the cf distribution.",
    Name=hosted_zone_param,
    Type="A",
    AliasTarget=r53.AliasTarget(
        DNSName=GetAtt("CFDistribution", "DomainName"),
        EvaluateTargetHealth=False,
        HostedZoneId=Ref(hosted_zone_id_param)
    )
))

cf_record = t.add_resource(r53.RecordSetType(
    "cfDNSRecord",
    HostedZoneName=Join("", [Ref(hosted_zone), "."]),
    Comment="dns name for the cf distribution.",
    Name=s3_dns,
    Type="CNAME",
    TTL="900",
    ResourceRecords=[GetAtt("CFDistribution", "DomainName")]
))

api_record = t.add_resource(r53.RecordSetType(
    "apiDNSRecord",
    HostedZoneName=Join("", [Ref(hosted_zone), "."]),
    Comment="dns name for the ec2 instance.",
    Name=api_dns,
    Type="A",
    TTL="900",
    ResourceRecords=[GetAtt("EC2Instance0", "PublicIp")]
))

## outputs ##
t.add_output(Output(
    "S3BucketDNOutput",
    Value=GetAtt("S3Bucket", "DomainName"),
    Description="DN of S3 bucket"
))

t.add_output(Output(
    "S3BucketURLOutput",
    Value=GetAtt("S3Bucket", "WebsiteURL"),
    Description="URL of S3 bucket"
))

t.add_output(Output(
    "CloudFrontURLOutput",
    Value=GetAtt("CFDistribution", "DomainName"),
    Description="URL of CF distribution"
))

for index, instance in enumerate(ec2_instances):
    t.add_output(Output(
        "EC2Instance{0}Output".format(index),
        Value=GetAtt(instance, "PublicIp"),
        Description="IP of EC2 instance {0}".format(index)
    ))

print(t.to_yaml())
