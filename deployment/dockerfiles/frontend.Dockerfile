### build stage ###
# label stage as build-env
FROM node:lts-alpine as build-env
WORKDIR /usr/src/app

# copy package.json and install dependencies
COPY frontend/package.json /usr/src/app
RUN npm install --silent

# copy everything else and build
COPY frontend /usr/src/app
RUN npx react-scripts build

### setup stage ###
FROM nginx:mainline-alpine
WORKDIR /usr/share/nginx/html

# copy custom nginx config
COPY nginx/frontend.conf /etc/nginx/conf.d/default.conf

# remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

# copy artifacts from build stage
COPY --from=build-env /usr/src/app/build /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
