### build stage ###
# label stage as build-env
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env
WORKDIR /usr/src/app

# Copy csproj and restore as distinct layers
COPY backend/EternalGroveApi/*.csproj /usr/src/app
RUN dotnet restore

# Copy everything else and build
COPY backend/EternalGroveApi /usr/src/app
RUN dotnet publish -c Release -o out

### setup stage ###
FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /usr/src/app

# copy artifacts from build stage
COPY --from=build-env /usr/src/app/out /usr/src/app

# set default port to 5000
ENV ASPNETCORE_URLS=http://+:5000

ENTRYPOINT ["dotnet", "/usr/src/app/EternalGroveApi.dll"]
